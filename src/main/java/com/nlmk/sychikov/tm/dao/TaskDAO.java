package com.nlmk.sychikov.tm.dao;

import com.nlmk.sychikov.tm.entity.Project;
import com.nlmk.sychikov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Tasks storage
 */
public class TaskDAO {

    private List<Task> tasks = new ArrayList<>();

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        return tasks;
    }

}
