package com.nlmk.sychikov.tm;

import com.nlmk.sychikov.tm.dao.ProjectDAO;
import com.nlmk.sychikov.tm.dao.TaskDAO;

import java.util.Scanner;

import static com.nlmk.sychikov.tm.constant.TerminalConst.*;

public class App {

    private static final ProjectDAO projectDAO = new ProjectDAO();

    private static final TaskDAO taskDAO = new TaskDAO();

    private static final Scanner scanner = new Scanner(System.in);

    /**
     * Main (start point)
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        run(args);
        displayWelcome();
        process();
    }

    /**
     * Command line args processor
     *
     * @param args command line arguments
     */
    private static void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * One command processor
     *
     * @param param command
     * @return return value
     */
    private static int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return exit();

            case PROJECT_CLEAR:
                return clearProject();
            case PROJECT_CREATE:
                return createProject();
            case PROJECT_LIST:
                return listProject();

            case TASK_CLEAR:
                return clearTask();
            case TASK_CREATE:
                return createTask();
            case TASK_LIST:
                return listTask();

            default:
                return displayError();
        }
    }

    /**
     * Add new project
     *
     * @return return value
     */
    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        final String name = scanner.nextLine();
        projectDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all projects
     *
     * @return return value
     */
    private static int clearProject() {
        System.out.println("[CLEAR PROJECTS]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all projects
     *
     * @return return value
     */
    private static int listProject() {
        System.out.println("[LIST PROJECTS]");
        System.out.println(projectDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Add new task
     *
     * @return return value
     */
    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter task name: ");
        final String name = scanner.nextLine();
        taskDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Clear all tasks
     *
     * @return return value
     */
    private static int clearTask() {
        System.out.println("[CLEAR TASKS]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * List all tasks
     *
     * @return return value
     */
    private static int listTask() {
        System.out.println("[LIST TASKS]");
        System.out.println(taskDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Infinite loop command processor with console input.
     * The "exit" command stops the loop.
     */
    private static void process() {
        String command = "";
        while (!EXIT.equals(command)) {
            System.out.print("command:> ");
            command = scanner.nextLine();
            run(command);
            System.out.println();
        }
    }

    /**
     * Show welcome string
     */
    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    /**
     * Show available command
     *
     * @return return value
     */
    private static int displayHelp() {
        System.out.println(VERSION + " - display the version info");
        System.out.println(HELP + " - display the list of terminal commands");
        System.out.println(ABOUT + " - display the developer info");
        System.out.println(EXIT + " - close the program");
        System.out.println();
        System.out.println(PROJECT_CREATE + " - create the new project");
        System.out.println(PROJECT_CLEAR + " - clear all projects");
        System.out.println(PROJECT_LIST + " - list all projects");
        System.out.println();
        System.out.println(TASK_CREATE + " - create the new task");
        System.out.println(TASK_CLEAR + " - clear all tasks");
        System.out.println(TASK_LIST + " - list all tasks");
        return 0;
    }

    /**
     * Show error message
     *
     * @return return value
     */
    private static int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    /**
     * Show version
     *
     * @return return value
     */
    private static int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Show exit string
     *
     * @return value
     */
    private static int exit() {
        System.out.println("Our program exit now...");
        System.out.println("Bye!");
        return 0;
    }

    /**
     * Show info
     *
     * @return return value
     */
    private static int displayAbout() {
        System.out.println("Vladimir Sychikov");
        System.out.println("VladimirSychikov@nospam.ru");
        return 0;
    }

}
